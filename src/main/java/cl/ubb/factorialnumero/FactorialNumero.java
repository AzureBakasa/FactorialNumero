package cl.ubb.factorialnumero;

public class FactorialNumero {

	
	public int devuelveFactorial(int numero) {
		if (numero==0)
		    return 1;
		  else
		    return numero*devuelveFactorial(numero-1);
		
	}
	
	/*public int devuelveFactorial(int i) {
		if(i==1 || i==0)
			return 1;
		if(i==2)
			return 2;
		if(i==3)
			return 6;
		else
			return -1;
	}*/

}
