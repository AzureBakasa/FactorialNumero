package cl.ubb.factorialnumero;

import static org.junit.Assert.*;

import org.junit.Test;

public class FactorialNumeroTest {

	@Test
	public void IngresaCeroRetornaUno() {
		//arrange
		FactorialNumero fact = new FactorialNumero();
		int resultado;
		//act
		resultado=fact.devuelveFactorial(0);
		//assert
		assertEquals(resultado,1);
	}
	@Test
	public void IngresaUnoRetornaUno() {
		//arrange
		FactorialNumero fact = new FactorialNumero();
		int resultado;
		//act
		resultado=fact.devuelveFactorial(1);
		//assert
		assertEquals(resultado,1);
	}
	@Test
	public void IngresaDosRetornaDos() {
		//arrange
		FactorialNumero fact = new FactorialNumero();
		int resultado;
		//act
		resultado=fact.devuelveFactorial(2);
		//assert
		assertEquals(resultado,2);
	}
	@Test
	public void IngresaTresRetornaSeis() {
		//arrange
		FactorialNumero fact = new FactorialNumero();
		int resultado;
		//act
		resultado=fact.devuelveFactorial(3);
		//assert
		assertEquals(resultado,6);
	}
	@Test
	public void IngresaCuatroRetornaVeiticuatro() {
		//arrange
		FactorialNumero fact = new FactorialNumero();
		int resultado;
		//act
		resultado=fact.devuelveFactorial(4);
		//assert
		assertEquals(resultado,24);
	}
	@Test
	public void IngresaCincoRetornaCientoveinte() {
		//arrange
		FactorialNumero fact = new FactorialNumero();
		int resultado;
		//act
		resultado=fact.devuelveFactorial(5);
		//assert
		assertEquals(resultado,120);
	}


}
